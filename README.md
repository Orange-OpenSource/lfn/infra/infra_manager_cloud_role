Infra manager cloud role
=========

Ansible galaxy role to setup openstack cloud config for infra_manager from an
openrc file

Requirements
------------

A valid openrc file

Role Variables
--------------

Environment variables:
  - ```LOCAL_CLOUD```: if different than NONE, path to a local cloud.yaml config
  to generate

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:


```
source vars/openstack_user_openrc
ansible-playbook ${ansible_verbose} -i inventory/inventory os_cloud.yml
```

```os_cloud.yml
- hosts: jumphost
  any_errors_fatal: true
  roles:
    - name: create the config in clouds.yaml
      role: cloud
```

License
-------

Apache

Author Information
------------------

David Blaisonneau
Sylvain Desbureau
